from setuptools import setup, find_packages

setup(
    name='FileManager',
    version='1.0.0',
    description='FileManager App',
    long_description='''
FileManager Server app
''',
    keywords='',
    author='DEV-ESG',
    author_email='13@1500.co.il',
    url='https://gitlab.com/esg-dev/file-producer.git',
    license='MIT',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: Unix',
        'Programming Language :: Python :: 2.7',
        'Operating System :: OS Independent',
    ],
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'openpyxl'
    ],
    entry_points={
        'console_scripts': [
        ],
    },
)
