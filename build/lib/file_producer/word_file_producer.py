# -*- coding: utf-8 -*-
from collections import OrderedDict
from abstract.file_producer import FileProducer
import json
import os
import uuid

class WordFileProducer(FileProducer):
    def __init__(self, output_path, output_file_name, template_dict, template_path, template_file_name):
        """
        :param path: init the output file path
        :param file_name: init the file name
        :param template_dict: init the dictionary of word params
        :param template_path: init the template file path
        :param template_file_name: init the template file name
        cunstructor
        """
        FileProducer.__init__(self, output_path, output_file_name)
        self.template_path = template_path
        self.template_file_name = template_file_name
        self.template_dict = template_dict
        self.configuration_dict = None


    def set_file_configurations(self, **kwargs):
        """
        set file configuartions
        :param kwargs:
        :return:
        """
        self.configuration_dict = OrderedDict([("config.inputFile", '{template_path}/{template_file_name}.docx'.format(template_path= self.template_path, template_file_name=self.template_file_name)),
                                    ("config.outputFile", '{full_path}.docx'.format(full_path=self.full_path)),
                                    ("config.qrcode", False),
                                    ("config.debug", False)])
        print '{full_path}.docx'.format(full_path=self.full_path)
        return self


    def produce_file(self):
        """
        create word file
        :return:
        """
        final_dict = dict(self.configuration_dict.items() + self.template_dict.items())
        js = json.dumps(final_dict, indent=4)
        with open('sample3.json', 'w') as outfile:
            outfile.write(js)
        os.system("docxtemplater sample3.json")




