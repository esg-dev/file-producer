# -*- coding: utf-8 -*-
import pandas as pd
from abstract.file_producer import FileProducer
class ExcelFileProducer(FileProducer):
    def __init__(self, output_path, output_file_name, styling_appliers_list, sheet_to_indents_dict, start_row):
        """
        :param path: init the output file path
        :param file_name: init the file name
        cunstructor
        """
        FileProducer.__init__(self, output_path, output_file_name)
        self.excel_writer = pd.ExcelWriter('{full_path}.xlsx'.format(full_path=self.full_path), engine="openpyxl")
        self.styling_appliers_list = styling_appliers_list
        self.start_row = start_row
        self.sheets_to_indents_dict = {}
        self.sheets_to_stylers_dict = {}
        self.__private_init_styling_appliers_excel_writer()
        self.__private_init_sheet_to_stylers_dict()
        self.__private_init_sheet_to_indents_dict(sheet_to_indents_dict)
        self.__private_init_data_frame_indents()
        self.__private_fix_applied_stylers_locations()

    def __private_init_styling_appliers_excel_writer(self):
        for styling_applier in self.styling_appliers_list:
            styling_applier.excel_writer = self.excel_writer

    def __private_init_data_frame_indents(self):
        for sheet_name in self.sheets_to_stylers_dict:
            indents_sum = 0 + self.start_row
            for styling_applier in self.sheets_to_stylers_dict[sheet_name]:
                if styling_applier.headline_data_frame is not None:
                    styling_applier.data_frame_headline_position = indents_sum
                    styling_applier.data_frame_position = indents_sum + 1
                    indents_sum += len(styling_applier.data_frame) + 2 + self.sheets_to_indents_dict[sheet_name]
                else:
                    styling_applier.data_frame_position = indents_sum
                    indents_sum += len(styling_applier.data_frame) + 1 + self.sheets_to_indents_dict[sheet_name]


    def __private_fix_applied_stylers_locations(self):
        for styling_applier in self.styling_appliers_list:
            for location_and_style in styling_applier.locations_and_styles_list:
                location_and_style['start_row'] = location_and_style['start_row'] + styling_applier.data_frame_position
                location_and_style['end_row'] = location_and_style['end_row'] + styling_applier.data_frame_position

    def __private_init_sheet_to_stylers_dict(self):
        sheet_names = set()
        for styling_applier in self.styling_appliers_list:
            sheet_names.add(styling_applier.sheet_name)
        self.sheets_to_stylers_dict = {sheet_name: [] for sheet_name in sheet_names}
        for sheet_name in self.sheets_to_stylers_dict:
            for styling_applier in self.styling_appliers_list:
                if sheet_name == styling_applier.sheet_name:
                    self.sheets_to_stylers_dict[sheet_name].append(styling_applier)

    def __private_init_sheet_to_indents_dict(self, sheet_to_indents_dict):
        for key in self.sheets_to_stylers_dict.keys():
            if key not in sheet_to_indents_dict.keys():
                raise Exception("sheet does not exists")
        self.sheets_to_indents_dict = sheet_to_indents_dict

    def set_file_configurations(self, **kwargs):
        """
        set file configuartions
        :param kwargs:
        :return:
        """
        pass

    def produce_file(self):
        """
        create excel file
        :return:
        """
        for sheet_name in self.sheets_to_stylers_dict:
            for styling_applier in self.sheets_to_stylers_dict[sheet_name]:
                styling_applier.apply_styling()
        self.excel_writer.save()
