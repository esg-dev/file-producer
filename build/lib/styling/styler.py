# -*- coding: utf-8 -*-
from openpyxl.styles import Style, Font, Border, Side, PatternFill, Alignment
from openpyxl.styles.colors import BLACK, RED, GREEN, BLUE, YELLOW
DEFAULT_STYLE_ESG_FONT = Font(name="Arial", size=12, color=BLACK, bold=False)
DEFAULT_STYLE_ESG_SIDE = Side(border_style="thin", color=BLACK)
DEFAULT_STYLE_ESG_BORDER = Border(left=DEFAULT_STYLE_ESG_SIDE, right=DEFAULT_STYLE_ESG_SIDE, top=DEFAULT_STYLE_ESG_SIDE,bottom=DEFAULT_STYLE_ESG_SIDE)
DEFAULT_STYLE_ESG_FILL = PatternFill(patternType='solid', fgColor=RED)
DEFAULT_STYLE_ESG_ALIGNMENT = Alignment(horizontal='center', vertical='center', wrap_text=True, shrink_to_fit=True, indent=0)
DEFAULT_STYLE_ESG = Style(font=DEFAULT_STYLE_ESG_FONT, border=DEFAULT_STYLE_ESG_BORDER, alignment=DEFAULT_STYLE_ESG_ALIGNMENT)

class Styler(object):
    def __init__(self):
        """
        default values
        :return:
        """
        self.esg_font_style = DEFAULT_STYLE_ESG_FONT
        self.esg_side_style = DEFAULT_STYLE_ESG_SIDE
        self.esg_border_style = DEFAULT_STYLE_ESG_BORDER
        self.esg_fill_style = None
        self.esg_alignment_style = DEFAULT_STYLE_ESG_ALIGNMENT
        self.esg_style = DEFAULT_STYLE_ESG
        self.colors_dict = {}
        self.set_colors_dict()

    def set_colors_dict(self):
        self.colors_dict = {'red': RED, 'blue': BLUE, 'yellow': YELLOW, 'green': GREEN, 'black': BLACK, 'orange': "FFA500", 'light_blue': "00FFFF"}

    def set_font(self, font_name, font_size, font_color, is_bold):
        """
        init font style
        :param font_name: string
        :param font_size: int
        :param font_color: string
        :return: self
        """
        self.esg_font_style = Font(name=font_name, size=font_size, color=self.colors_dict[font_color], bold=is_bold)
        return self

    def set_side(self, border_style, border_color):
        """
        init table side style
        :param border_style: string
        :param border_color: string
        :return:self
        """
        self.esg_side_style = Side(border_style=border_style, color=border_color)
        return self

    def set_border(self):
        """
        init all table sides by the side object was initiated
        :return: self
        """
        self.esg_border_style = Border(left=self.esg_side_style, right=self.esg_side_style, top=self.esg_side_style, bottom=self.esg_side_style)
        return self

    def set_fill(self, pattern_type, fill_color):
        """
        set cell color fill
        set the fill style for cell
        :param pattern_type: string
        :param fill_color: string
        :return:self
        """
        self.esg_fill_style = PatternFill(patternType=pattern_type, fgColor=self.colors_dict[fill_color])
        return self

    def set_alignment(self, horizontal, vertical, wrap_text, shrink_to_fit, indent):
        """
        :param horizontal: string
        :param vertical: string
        :param wrap_text: boolean
        :param shrink_to_fit: boolean
        :param indent: int
        :return:self
        """
        self.esg_alignment_style = Alignment(horizontal=horizontal, vertical=vertical, wrap_text=wrap_text, shrink_to_fit=shrink_to_fit, indent=indent)
        return self

    def get_style(self):
        """
        get the created style
        :return: style
        """
        if self.esg_fill_style is not None:
            self.esg_style = Style(font=self.esg_font_style, fill=self.esg_fill_style, border=self.esg_border_style, alignment=self.esg_alignment_style)
        else:
            self.esg_style = Style(font=self.esg_font_style, border=self.esg_border_style, alignment=self.esg_alignment_style)
        return self.esg_style


