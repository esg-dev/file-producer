# -*- coding: utf-8 -*-
import pandas as pd
from styling.styler import Styler

class StylingApplier(object):
    def __init__(self, data_frame, sheet_name):
        self.data_frame = data_frame
        self.sheet_name = sheet_name
        self.data_frame_configurations = None
        self.excel_writer = None
        self.data_frame_headline_position = None
        self.data_frame_position = 0
        self.headline_data_frame = None
        self.headline_style = None
        self.locations_and_styles_list = []
        self.init_default_configurations()


    def init_default_configurations(self):
        default_location_and_style = {'start_row': 1, 'end_row': len(self.data_frame) + 2, 'start_col': 1, 'end_col': len(self.data_frame.columns) + 1, 'style': Styler().get_style()}
        self.locations_and_styles_list.append(default_location_and_style)
        self.data_frame_configurations = {'index': False, 'sheet_name': self.sheet_name}

    def set_data_frame_configuarations(self, **kwargs):
        """
        set the data frame configuartions for example startrow, startcol, index
        :param kwargs:
        :return:self
        """
        self.data_frame_configurations = kwargs
        return self


    def set_headline(self, headline, style=Styler().get_style()):
        self.headline_data_frame = pd.DataFrame({headline: []})
        self.headline_style = style
        return self

    def set_locations_and_style(self, start_row, end_row, start_col, end_col, style=Styler().get_style()):
        """
        this function take 5 paramters, the first 4 paramters indicate the location for styling
        and the last parameter indicate a style object that will be applied on the selected cells
        the function add the data to a list of locations and styles
        :param start_row:
        :param end_row:
        :param start_col:
        :param end_col:
        :param style:
        :return:
        """
        new_location_and_style = {'start_row': start_row, 'end_row': end_row, 'start_col': start_col, 'end_col': end_col, 'style': style}
        self.locations_and_styles_list.append(new_location_and_style)
        return self


    def apply_headline_style(self, ws):
        """
        applying the header style
        :param ws:
        :return:
        """
        headline_cell = ws.cell(row=self.data_frame_headline_position + 1, column=1)
        headline_cell.style = self.headline_style



    def apply_content_style(self, ws):
        """
        applying the content style
        :param ws:
        :return:
        """
        for location_and_style in self.locations_and_styles_list:
            for i in xrange(location_and_style['start_row'], location_and_style['end_row']):
                for j in xrange(location_and_style['start_col'], location_and_style['end_col']):
                    cell = ws.cell(row=i, column=j)
                    cell.style = location_and_style['style']



    def apply_styling(self):
        """
        this function responsible to loop over the locations and style list and apply the style on the selected cells
        :return:
        """
        if self.headline_data_frame is not None:
            self.headline_data_frame.to_excel(self.excel_writer, sheet_name=self.data_frame_configurations['sheet_name'], startrow=self.data_frame_headline_position, startcol=0, index=self.data_frame_configurations['index'])
        self.data_frame.to_excel(self.excel_writer, sheet_name=self.data_frame_configurations['sheet_name'], startrow=self.data_frame_position, startcol=0, index=self.data_frame_configurations['index'])
        ws = self.excel_writer.sheets[self.sheet_name]
        ws.sheet_view.rightToLeft = True
        if self.headline_data_frame is not None:
            self.apply_headline_style(ws)
        self.apply_content_style(ws)







