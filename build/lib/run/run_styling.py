from file_producer.excel_file_producer import ExcelFileProducer
from styling.styling_applier import StylingApplier
from styling.styler import Styler
import pandas as pd


colors_dict = {'red': 'blue', 'blue' : 'red'}
LIST_OF_APPLYERS = []
for k in range(6):
    df_dict = {i: range(20) for i in range(10)}
    df = pd.DataFrame(df_dict)
    header_style = Styler().set_fill(pattern_type='solid', fill_color='light_blue').get_style()
    applier = StylingApplier(df, 'gal').set_locations_and_style(1, 2, 1, len(df.columns)+1, header_style)
    current_color = 'red'
    for i in range(2, len(df)+2):
        for j in range(1, len(df.columns) + 1):
            cell_style = Styler().set_fill(pattern_type='solid', fill_color=current_color).get_style()
            applier.set_locations_and_style(i, i+1, j, j+1, cell_style)
            current_color = colors_dict[current_color]
    LIST_OF_APPLYERS.append(applier)


for m in range(6):
    df_dict = {i: range(20) for i in range(10)}
    df = pd.DataFrame(df_dict)
    header_style = Styler().set_fill(pattern_type='solid', fill_color='light_blue').get_style()
    applier = StylingApplier(df, 'gal2').set_locations_and_style(1, 2, 1, len(df.columns)+1, header_style)
    current_color = 'red'
    for i in range(2, len(df)+2):
        for j in range(1, len(df.columns) + 1):
            cell_style = Styler().set_fill(pattern_type='solid', fill_color=current_color).get_style()
            applier.set_locations_and_style(i, i+1, j, j+1, cell_style)
            current_color = colors_dict[current_color]
    LIST_OF_APPLYERS.append(applier)

for s in range(6):
    df_dict = {i: range(20) for i in range(10)}
    df = pd.DataFrame(df_dict)
    header_style = Styler().set_fill(pattern_type='solid', fill_color='light_blue').get_style()
    applier = StylingApplier(df, 'gal3').set_locations_and_style(1, 2, 1, len(df.columns)+1, header_style)
    current_color = 'red'
    for i in range(2, len(df)+2):
        for j in range(1, len(df.columns) + 1):
            cell_style = Styler().set_fill(pattern_type='solid', fill_color=current_color).get_style()
            applier.set_locations_and_style(i, i+1, j, j+1, cell_style)
            current_color = colors_dict[current_color]
    LIST_OF_APPLYERS.append(applier)


producer = ExcelFileProducer('C:/Users/DEV/PycharmProjects/FIleManager/run', 'output', LIST_OF_APPLYERS, {'gal': 5, 'gal2': 2, 'gal3': 4})
producer.produce_file()

