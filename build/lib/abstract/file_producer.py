# -*- coding: utf-8 -*-
class FileProducer(object):
    def __init__(self, output_path, output_file_name):
        self.output_path = output_path
        self.output_file_name = output_file_name
        self.full_path = '{output_file_name}'.format(output_path=self.output_path, output_file_name=self.output_file_name)


    def set_file_configurations(self, **kwargs):
        pass

    def produce_file(self):
        pass